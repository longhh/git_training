package com.example.a02activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnOpenURL, btnNhapURL;
    TextView txtURL;
    int RESULT_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtURL = findViewById(R.id.textViewURL);
        btnOpenURL = findViewById(R.id.buttonOpenURL);
        btnNhapURL = findViewById(R.id.buttonNhapURL);

        btnNhapURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityNhap.class);
                startActivityForResult(intent, RESULT_CODE);
            }
        });

        btnOpenURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(txtURL.getText().toString()));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK & requestCode == RESULT_CODE && data != null) {
            String duongdan = data.getStringExtra("duongdan");
            txtURL.setText(duongdan);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}