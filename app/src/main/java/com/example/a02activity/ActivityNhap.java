package com.example.a02activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityNhap extends AppCompatActivity {

    Button btnOk;
    EditText edtURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nhap);

        btnOk = findViewById(R.id.buttonOk);
        edtURL = findViewById(R.id.editTextURL);

        btnOk.setText("123");
        btnOk.setText("456");

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String URL = edtURL.getText().toString().trim();
                Intent intent = new Intent();
                intent.putExtra("duongdan", URL);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}